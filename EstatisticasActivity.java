package com.br.qualconcurso;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.br.qualconcurso.classes.DatabaseHelper;
import com.br.qualconcurso.classes.Filtro;
import com.br.qualconcurso.classes.Questao;
import com.br.qualconcurso.estatisticasFragment.AssuntoFragment;
import com.br.qualconcurso.estatisticasFragment.GeralFragment;

import java.util.ArrayList;


public class EstatisticasActivity extends ActionBarActivity implements ActionBar.TabListener  {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    //private NavigationDrawerFragment mNavigationDrawerFragment;
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    ViewPager mViewPager;

    private CharSequence mTitle;
    public ArrayList<Questao> questoesRespondidas;
    public ArrayList<String> assuntosQuestoes;
    public ArrayList<Float> assuntosPercentages;

    public float acertoLiquido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());
//        mAppSectionsPagerAdapter.;
        mAppSectionsPagerAdapter.setTabTitles(
                new String[] {
                        getString(R.string.title_estatistica1),
                        getString(R.string.title_estatistica2)
                }
        );
        DatabaseHelper db = new DatabaseHelper(getApplicationContext());
        questoesRespondidas = db.getQuestoesByFiltro(Filtro.savedFilter);
        acertoLiquido = calcularAcertoLiquido();
        assuntosQuestoes = new ArrayList<String>();
        assuntosPercentages = new ArrayList<Float>();
        db.getAssuntosPercentages(assuntosQuestoes, assuntosPercentages, Filtro.savedFilter);
        db.close();
        setContentView(R.layout.activity_estatisticas);
        //final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        final Toolbar actionBar = (Toolbar) findViewById(R.id.toolbar);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            actionBar.setElevation(0);
        }
        actionBar.setTitle("   " + getResources().getString(R.string.title_activity_estatisticas));
        //actionBar.setLogo(getResources().getDrawable(R.drawable.logo_largo2));
        actionBar.setTitleTextColor(Color.WHITE);
        actionBar.setNavigationIcon(getResources().getDrawable(R.drawable.arrow_left));
        actionBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Specify that the Home/Up button should not be enabled, since there is no hierarchical
        // parent.
        if (actionBar != null) {
            //actionBar.setHomeButtonEnabled(false);
            //actionBar.setElevation(0);
        }

        // Specify that we will be displaying tabs in the action bar.
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Set up the ViewPager, attaching the adapter and setting up a listener for when the
        // user swipes between sections.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        final PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs_estatisticas);
        //final HorizontalScrollView horizontalScrollView = (HorizontalScrollView) findViewById(R.id.hsv);
        tabs.setViewPager(mViewPager);
        tabs.setSmoothScrollingEnabled(true);
        tabs.setHorizontalFadingEdgeEnabled(true);
        tabs.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When swiping between different app sections, select the corresponding tab.
                // We can also use ActionBar.Tab#select() to do this if we have a reference to the
                // Tab.
                //actionBar.setSelectedNavigationItem(position);
                //tabs.select
                //int x = tabs.;
                //int y =  horizontalScrollView.getScrollY();
                //horizontalScrollView.smoothScrollTo(x , y);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        if(mAppSectionsPagerAdapter != null){
            for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
                // Create a tab with text corresponding to the page title defined by the adapter.
                // Also specify this Activity object, which implements the TabListener interface, as the
                // listener for when this tab is selected.
                //actionBar.addTab(
                        //actionBar.newTab()
                                //.setText(mAppSectionsPagerAdapter.getPageTitle(i))
                                //.setTabListener(this));
            }
        }

        }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        /*ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

    }


    /**** INNER CLASSES ****/

    /**
     * A dummy fragment representing a section of the app, but that simply displays dummy text.
     */
    public static class DummySectionFragment extends Fragment {

        public static final String ARG_SECTION_NUMBER = "section_number";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_section_dummy, container, false);
            Bundle args = getArguments();
            ((TextView) rootView.findViewById(android.R.id.text1)).setText("EM BREVE!");
            return rootView;
        }
    }

    static class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        private String tabTitles[] = new String[] { "Tab1", "Tab2"};
        private Fragment fragments[] = new Fragment[] {new GeralFragment(), new AssuntoFragment()};



        public void setTabTitles(String [] strings){
            tabTitles = strings;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return fragments[i];

                case 1:
                    return fragments[i];

                default:
                    // The other sections of the app are dummy placeholders.
                    DummySectionFragment fragment = new DummySectionFragment();
                    Bundle args = new Bundle();
                    args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
                    fragment.setArguments(args);
                    return fragment;
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            //super.destroyItem(container, position, object);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }

    public float calcularAcertoLiquido() { //Cálculo não conta com "Pulo" de questão
        int n[] = {0, 0, 0}; //Respondidas
        int k[] = {0, 0, 0}; //Acertos
        float a[] = {0, 0, 0};
        int m[] = {2, 4, 5};
        float ar, nr;
        int i;
        if(Filtro.tipoQuestao == null || Filtro.tipoQuestao.equals("") || Filtro.tipoQuestao.equals("0")){
            for(i = 0; i < questoesRespondidas.size(); i++){
                Questao questao = questoesRespondidas.get(i);
                boolean correta = questao.getCorreta().equals("true");
                int tipo = getTipoRespostas(questao.getQuantOpcoes());
                n[tipo]++;
                k[tipo] += correta ? 1 : 0 ;
            }
            for(i = 0; i < 3; i++){
                a[i] = n[i] == 0 ? 0 : k[i] / (float)(n[i]);
                a[i] = (((a[i]*m[i])-1)/(m[i]-1));
            }


            ar = (n[0]*a[0]) + (n[1]*a[1])+(n[2]*a[2])/(n[0]+n[1]+n[2]);
            nr = questoesRespondidas.size();
            ar = (ar*(n[0]+n[1]+n[2]))/nr;
        } else {
            int tipo = 0;
            for(i = 0; i < questoesRespondidas.size(); i++){
                Questao questao = questoesRespondidas.get(i);
                boolean correta = questao.getCorreta().equals("true");
                tipo = getTipoRespostas(questao.getQuantOpcoes());
                n[tipo]++;
                k[tipo] += correta ? 1 : 0 ;
            }
            ar = k[tipo] / (float)n[tipo];
            a[tipo] = (((ar*m[tipo])-1)/(float)(m[tipo]-1));
            ar = (a[tipo]*n[tipo])/(float)n[tipo];
        }


        return ar;

    }

    private int getTipoRespostas(int i){
        switch (i){
            case 2: return 0;
            case 4: return 1;
            case 5: return 2;
        }
        return 0;
    }



    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }else{
            Toast.makeText(EstatisticasActivity.this, "É necessário uma conexão com a internet.", Toast.LENGTH_LONG).show();
        }
        return false;
    }



}
