# -*- coding: utf-8 -*-
from django.http import HttpResponse
import json
from django.views.decorators.csrf import csrf_exempt
from fast.models import Usuario

@csrf_exempt
def register_user(request):

    ret = None
    data = None
    if request.method != 'POST':
        ret = json.dumps({"is_registered": False})
    else:
        data = json.loads(request.POST['registerUser'])
        if data['action'] != 'save' or data['gcm_id'] is "" or data['name'] is "" or data['email'] is None:
            ret = json.dumps({"is_registered": False})

    if ret is None and data is not None:
        try:
            new_user = Usuario(name=data['name'], email=data['email'], gcm_id=data['gcm_id'])
            new_user.save()
            ret = json.dumps({"is_registered": True, "name": new_user.name, "email": new_user.email, "gcm_id": new_user.gcm_id})

        except Exception as e:
            ret = json.dumps({"is_registered": False})

    return HttpResponse(ret)

@csrf_exempt
def remove_user(request):

    ret = None
    data = None
    if request.method != 'POST':
        ret = json.dumps({"is_removed": False})
    else:
        data = json.loads(request.POST['removeUser'])
        if data['action'] != 'remove' or data['gcm_id'] is "":
            ret = json.dumps({"is_removed": False})

    if ret is None and data is not None:
        try:
            rm_user = Usuario.objects.get(gcm_id=data['gcm_id'])
            rm_user.delete()
            ret = json.dumps({"is_removed": True})

        except Exception as e:
            ret = json.dumps({"is_removed": False})

    return HttpResponse(ret)
