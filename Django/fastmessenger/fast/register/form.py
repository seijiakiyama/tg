# -*- coding: utf-8 -*-

import django.forms as forms
from fast.models import Usuario

__author__ = "Andre Pereira <alalvespereira@gmail.com>"

class Form(forms.ModelForm):
    class Meta():
        model = Usuario
        fields = Usuario._meta.get_all_field_names()
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(Form, self).__init__(*args, **kwargs)


class PartialForm(forms.ModelForm):

    required_css_class = 'required'

    class Meta():
        model = Usuario
        fields = Usuario._meta.get_all_field_names()
        fields = '__all__'
        exclude = ['adicionado']
