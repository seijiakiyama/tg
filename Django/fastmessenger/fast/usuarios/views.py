# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
import json
from django.views.decorators.csrf import csrf_exempt
from fast.models import Usuario
from fast.register.form import PartialForm

@csrf_exempt
def get_all_users(request):

    ret = None
    if request.method != 'GET':
        ret = json.dumps({"is_get_all_user": False})

    if ret is None:
        try:
            users = Usuario.objects.all().exclude(gcm_id=request.GET['regid'])
            users_dict = {"is_get_all_user": True}
            users_ids = []
            for user in users:
                tempDict = {"name": user.name, "email": user.email, "gcm_id": user.gcm_id}
                users_ids.append(tempDict)

            users_dict["objects"]=users_ids

            ret = json.dumps(users_dict)

        except Exception as e:
            ret = json.dumps({"is_get_all_user": False})

    return HttpResponse(ret)
