#!/usr/bin/python
import random
import string
import sys, json, xmpp
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

SERVER = 'gcm.googleapis.com'
PORT = 5235
USERNAME = "525445883554" #"Your GCM Sender Id"
PASSWORD = "AIzaSyBw8rINewdU6Gcr3pOh7XxF6ykEEhlTr1g" #"API Key"
REGISTRATION_ID = "" #"Registration Id of the target device"

unacked_messages_quota = 100
send_queue = []

#client = xmpp.Client('gcm.googleapis.com', debug=['socket'])
client = xmpp.Client('gcm.googleapis.com')
client.connect(server=(SERVER, PORT), secure=1, use_srv=False)
auth = client.auth(USERNAME, PASSWORD)
if not auth:
    print 'Authentication failed!'
    sys.exit(1)

@csrf_exempt
def send_now(request):

    ret = None
    if request.method != 'POST':
        ret = json.dumps({"is_send_message": False})

    elif ret is None:
        data = json.loads(request.POST['sendNowUser'])
        if data['regid'] is "" or data['message'] is "":
            ret = json.dumps({"is_send_message": False})

        else:
            try:
                regid = data['regid']
                #fromRegId = data['fromRegId']
                message = data['message']
                dateMessage = data['dateMessage']
                message_id = random_id()

                client.RegisterHandler('message', message_callback)

                send_queue.append({'to': regid,
                   'message_id': 'reg_id',
                   'data': {'message_destination': 'RegId',
                            'message_id': message_id,
                            'message': message,
                            'dateMessage': dateMessage}})

                #send_queue.append({'to': regid,
                #                   'data': {'message_destination': 'RegId',
                #                            'message_id': message_id,
                #                            'message': message,
                #                            'fromRegId': fromRegId,
                #                           }
                #                 })

                client.Process(1)
                flush_queued_messages()

                ret = json.dumps({"is_send_message": True})

            except Exception as e:
                ret = json.dumps({"is_send_message": False})
                client.disconnected()

    return HttpResponse(ret)

def message_callback(session, message):
    global unacked_messages_quota
    gcm = message.getTags('gcm')
    if gcm:
        gcm_json = gcm[0].getData()
        msg = json.loads(gcm_json)
        if not msg.has_key('message_type'):
            # Acknowledge the incoming message immediately.
            send({'to': msg['from'],
                  'message_type': 'ack',
                  'message_id': msg['message_id']})
            # Queue a response back to the server.
            if msg.has_key('from'):
              # Send a dummy echo response back to the app that sent the upstream message.
              send_queue.append({'to': msg['from'],
                                 'message_id': random_id(),
                                 'data': {'pong': 1}})
        elif msg['message_type'] == 'ack' or msg['message_type'] == 'nack':
            unacked_messages_quota += 1

def send(json_dict):
    template = "<message><gcm xmlns='google:mobile:data'>{1}</gcm></message>"
    client.send(xmpp.protocol.Message(node=template.format(client.Bind.bound[0], json.dumps(json_dict))))

def flush_queued_messages():
    global unacked_messages_quota
    while len(send_queue) and unacked_messages_quota > 0:
        send(send_queue.pop(0))
        unacked_messages_quota -= 1


# Return a random alphanumerical id
def random_id():
    #rid = uuid.uuid1()
    rid = ''
    for x in range(8):
        rid += random.choice(string.ascii_letters + string.digits)
    return rid