# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext


def test(request):
    url_action = '/send_now/'
    method = 'post'
    return render_to_response('test/index.html',
                              {'url_action':url_action,
                               'method':method},
                              RequestContext(request))