from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin
from fast.models import Usuario


class CustomUsuariosAdmin(ModelAdmin):
    list_display = ("name", "email")
    search_fields = ("name", "email")
    list_filter = ("name", "email")

admin.site.register(Usuario, CustomUsuariosAdmin)