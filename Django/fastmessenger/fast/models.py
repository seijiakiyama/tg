# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.db.models import Model, CharField, DateTimeField

# Create your models here.
class Usuario(Model):
    adicionado = DateTimeField(auto_now_add=True)
    name = CharField(max_length=100, null=False, blank=False, verbose_name=u'Name')
    email = CharField(max_length=100, null=True, blank=True, verbose_name=u'Email')
    gcm_id = CharField(max_length=500, null=False, blank=False, verbose_name=u'GCM ID')

    def __unicode__(self):
        return u'{}'.format(self.name,)

    class Meta():
        verbose_name_plural = u'Usuários'
        verbose_name = u'Usuário'