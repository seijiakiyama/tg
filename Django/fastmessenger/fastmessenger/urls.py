from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'fastmessenger.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/?', include(admin.site.urls)),

    url(r'^register/?', 'fast.register.views.register_user', name='register_user'),
    url(r'^remove/?', 'fast.register.views.remove_user', name='remove_user'),

    url(r'^get_all_users/?', 'fast.usuarios.views.get_all_users', name='get_all_users'),

    url(r'^send_now/?', 'fast.views.send_now', name='send_now'),

    url(r'^test/?', 'fast.test.views.test', name='test'),
)
